﻿#ifndef _STANDART_SETTINGS_H_
#define _STANDART_SETTINGS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>
#include <vector>
#include <map>

typedef unsigned int uint;
typedef unsigned short ushort;
typedef std::vector<char> byteArr;
typedef std::map<ushort, void*> mapUshortToPtr;


class StantardSettings
{
public:
	StantardSettings();
	virtual ~StantardSettings();

public:
	bool Load(const std::string& lpszProfileName);
	bool Save(const std::string& lpszProfileName);
	void MakeStatusMsg();
	void UpdateStatusMsg(unsigned char);
public:
	uint m_dwPollingPeriod;
	bool m_bTestLoopback;
	bool m_bShowSIOMessages;
	bool m_bShowMessageErrors;
	bool m_bShowCOMErrors;
	std::string m_strSettingsReportPath;

	uint m_nBufferSize;

	std::string m_strIncomingAddress;
	uint m_nIncomingPort;

	std::string m_strCOMSetup;
	int m_iCOMRttc;
	int m_iCOMWttc;
	int m_iCOMRit;

	byteArr m_arPrefix;
	byteArr m_arOutPrefix;

	ushort m_wComposedType;
	ushort m_wOutputComposedType;
	ushort m_wCRC16Init;
	ushort m_wCPAddr;
	ushort m_wPUAddr;
	typedef struct tagMESSAGETYPE
	{
		ushort m_wType;			
		ushort m_wMaxLength;
		ushort m_wDestination;
		ushort m_wSource;
		ushort m_wDestMask;	
		ushort m_wSrcMask;
		tagMESSAGETYPE(ushort wType = 0, ushort wMaxLength = 0, ushort wDestination = 0, ushort wSource = 0, ushort wDestMask = 0, ushort wSrcMask = 0)
		{
			m_wType = wType;
			m_wMaxLength = wMaxLength;
			m_wDestination = wDestination;
			m_wSource = wSource;
			m_wDestMask = wDestMask;
			m_wSrcMask = wSrcMask;
		}
	} MESSAGETYPE, * PMESSAGETYPE;

	std::map<ushort, MESSAGETYPE> m_mapMsgTypes;
	mapUshortToPtr m_mapMsgTypesToUnpack;
	bool m_bUnpackAll;
	mapUshortToPtr m_mapMsgTypesToMark;
	bool m_bMarkAll;

	uint m_nStatusPeriod;
	int m_iSendStatTO;
	MESSAGETYPE m_StatusHdr;
	MESSAGETYPE m_StatusMsg;
	MESSAGETYPE m_MarkNestedMask;
	MESSAGETYPE m_MarkComposedMask;
	ushort m_TUType;
	ushort m_TUSrcMask;
	bool m_TUSrcComMsgIndex;
	uint m_TUPrimToSecSrc;
	uint m_TUSecToPrimSrc;

	byteArr m_arStatusData;
	byteArr m_arStatusMsg;

	bool m_bKeepLog;
	ushort m_wLogComposedType;
	mapUshortToPtr m_mapLogMsgTypesToUnpack;
	bool m_bLogUnpackAll;

	ushort m_wLogComposedTypeToPack;
	mapUshortToPtr m_mapLogMsgTypesToPack;
	bool m_bLogPackAll;

	//������ ugs
	ushort m_wSourceID;
	ushort m_wStatusRequestMessageType;
};

extern StantardSettings s_Settings;

#endif // _SETTINGS_H_
